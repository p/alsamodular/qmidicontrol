<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Gui</name>
    <message>
        <source>Error creating sequencer port.</source>
        <translation type="obsolete">Fehler beim Erzeugen des Sequenzer-Anschlusses</translation>
    </message>
    <message>
        <location filename="../gui.cpp" line="76"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Ctrl+Q</source>
        <comment>File|Quit</comment>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;About %1...</source>
        <translation>&amp;Über %1...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error opening ALSA sequencer.</source>
        <translation type="vanished">Fehler beim Öffnen des ALSA-Sequenzers.</translation>
    </message>
    <message>
        <source>Error creating sequencer port.</source>
        <translation type="vanished">Fehler beim Erzeugen des Sequenzer-Anschlusses</translation>
    </message>
</context>
</TS>
