<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Gui</name>
    <message>
        <location filename="../gui.cpp" line="76"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Ctrl+Q</source>
        <comment>File|Quit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;About %1...</source>
        <translation>&amp;A propos de %1...</translation>
    </message>
</context>
</TS>
