#include <QLayout>
#include <qcheckbox.h>
#include <qlabel.h>
#include <qmainwindow.h>
#include <qprocess.h>
#include <qpushbutton.h>
#include <qslider.h>
#include <qsocketnotifier.h>
#include <qstring.h>
#include <QtGlobal>

#include "gui.h"


Gui::Gui(int controller_count, int p_channel, int p_offset,
        QWidget *parent) : QWidget(parent), channel(p_channel),
    offset(p_offset)
{
    int l1;
    QString qs;
    static const char portname[] = PACKAGE "_out";

    if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
        qWarning("Error creating sequencer port.");
    }
    snd_seq_set_client_name(seq_handle, PACKAGE);
    if ((port_out = snd_seq_create_simple_port(seq_handle, portname,
                    SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
                    SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
        qWarning("Error creating sequencer port.");
    }

    QHBoxLayout *guiLayout = new QHBoxLayout;
    guiLayout->addSpacing(8);
    aboutWidget = new QMessageBox(this);

    for (l1 = 0; l1 < controller_count; l1++) {
        QLabel *numLabel = new QLabel(this);
        qs.sprintf("%2d", l1 + offset);
        numLabel->setText(qs);
        QSlider *slider = new QSlider(Qt::Vertical, this);
        slider->setRange(0,127);
        slider->setMinimumSize(10,127);
        sliderList.append(slider);
        /*
           QLabel *valLabel = new QLabel(sliderBox);
           qs.sprintf("0");
           valLabel->setText(qs);
           valLabelList.append(valLabel);
           */
        QObject::connect(slider, SIGNAL(valueChanged(int)), this,
                SLOT(updateValue(int)));
        slider->setTickInterval(6);
        slider->setTickPosition(QSlider::TicksLeft);

        QVBoxLayout *sliderBoxLayout = new QVBoxLayout;
        sliderBoxLayout->setSpacing(2);
        sliderBoxLayout->addWidget(slider);
        sliderBoxLayout->addWidget(numLabel);
        guiLayout->addLayout(sliderBoxLayout);
        guiLayout->addSpacing(8);
    }

    guiLayout->addWidget(aboutWidget);
    guiLayout->setSpacing(2);
    guiLayout->setMargin(2);
    setLayout(guiLayout);
}

Gui::~Gui()
{
}

void Gui::displayAbout()
{
    aboutWidget->about(this, tr("About %1").arg(PACKAGE), aboutText);
    aboutWidget->raise();
}

void Gui::updateValue(int val)
{
    snd_seq_event_t ev;
    int l1;
    QSlider *senderSlider;

    senderSlider = (QSlider *)sender();
    for (l1 = 0; l1 < sliderList.count(); l1++) {
        if (sliderList.at(l1) == senderSlider) {
            break;
        }
    }
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_subs(&ev);
    snd_seq_ev_set_direct(&ev);
    snd_seq_ev_set_source(&ev, port_out);
    ev.type = SND_SEQ_EVENT_CONTROLLER;
    ev.data.control.channel = channel;
    ev.data.control.param = l1 + offset;
    ev.data.control.value = val;
    snd_seq_event_output_direct(seq_handle, &ev);
}
