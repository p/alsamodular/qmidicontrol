#include <getopt.h>
#include <iostream>
#include <qapplication.h>
#include <qmainwindow.h>
#include <qmenubar.h>
#include <qmenu.h>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>

#include "gui.h"
#include "config.h"


static struct option options[] = {
    {"help", 0, 0, 'h'},
    {"channel", 1, 0, 'c'},
    {"num_controller", 1, 0, 'n'},
    {"offset", 1, 0, 'o'},
    {0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
    int getopt_return;
    int option_index;
    int offset = 1;
    int channel = 0;
    int number = 16;

    while((getopt_return = getopt_long(argc, argv, "hc:o:n:", options,
                    &option_index)) >= 0) {
        switch(getopt_return) {
            case 'c':
                channel = atoi(optarg);
                break;
            case 'o':
                offset = atoi(optarg);
                break;
            case 'n':
                number = atoi(optarg);
                break;
            case 'h':
                std::cout << std::endl << aboutText << std::endl;
                std::cout << "--channel <num>          MIDI Channel [0]" << std::endl;
                std::cout << "--num_controller <num>   Number of Controllers [16]" << std::endl;
                std::cout << "--offset <num>           Offset for Controller [1]" << std::endl << std::endl;
                exit(EXIT_SUCCESS);
        }
    }

    QApplication app(argc, argv);

    /* translator for system messages*/
    QTranslator qtTranslator;
    QLocale loc = QLocale::system();
    qtTranslator.load("qt_" + QLocale::system().name(),
            QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    // translator for qmidicontrol messages
    QTranslator qmidicontrolTr;
    if (qmidicontrolTr.load(QString(PACKAGE "_") + loc.name(), TRANSLATIONSDIR))
        app.installTranslator(&qmidicontrolTr);


    QMainWindow *top = new QMainWindow();
    top->setWindowTitle(PACKAGE);

    Gui *gui = new Gui(number, channel, offset, top);
    QMenuBar *menuBar = new QMenuBar;
    QMenu *filePopup = new QMenu(QMenu::tr("&File"),top);
    QMenu *aboutMenu = new QMenu(QMenu::tr("&Help"),top);
    filePopup->addAction(QMenu::tr("&Quit"), &app, SLOT(quit()),
            QKeySequence(QMenu::tr("Ctrl+Q", "File|Quit")));
    aboutMenu->addAction(QMenu::tr("&About %1..."). arg(PACKAGE), gui,
            SLOT(displayAbout()));
    menuBar->addMenu(filePopup);
    menuBar->addMenu(aboutMenu);

    top->setMenuBar(menuBar);
    top->setCentralWidget(gui);
    top->show();
    return app.exec();
}
