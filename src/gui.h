#ifndef GUI_H
#define GUI_H

#include <qstring.h>
#include <qlabel.h>
#include <qslider.h>
#include <qmessagebox.h>
#include <qlist.h>
#include <alsa/asoundlib.h>

#include "config.h"

static const char aboutText[] =
     "QMidiControl 0.0.2\n"
     "(C) 2002 Matthias Nagorni (SuSE AG Nuremberg)\n"
     "(C) 2009 Frank Kober\n"
     "(C) 2009 Guido Scholz\n\n"
     PACKAGE " is licensed under the GPL.\n";

class Gui : public QWidget
{
  Q_OBJECT

  private:
    QMessageBox *aboutWidget;
    QList<QSlider*> sliderList;
    snd_seq_t *seq_handle;
    int port_out, channel, offset;

  public:
    Gui(int controller_count, int p_channel, int p_offset, QWidget* parent=0);
    ~Gui();

  public slots:
    void displayAbout();
    void updateValue(int val);
};

#endif
